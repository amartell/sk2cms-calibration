import sys, os
import numpy as np
import pandas as pd

from root_pandas import read_root

branches = [u'event',
            u'rechit_layer', u'rechit_chip', u'rechit_channel',
            #u'rechit_x', u'rechit_y', u'rechit_z',
            u'rechit_energy', #u'rechit_TS3High', u'rechit_TS3Low',
            #u'rechit_amplitudeHigh',
            u'rechit_amplitudeLow', u'rechit_Tot',
            #u'rechit_time', u'rechit_timeMaxHG',
            u'rechit_timeMaxLG',
            u'rechit_toaRise', u'rechit_toaFall']

rh_branches = [branch for branch in branches if "rechit" in branch]

def process(fname = "/Users/artur/cernbox/HGCAL/testbeam/CERN2018/june/data/ntuples/ele100GeV/ntuple_381.root"):

    print('Processing file ' + fname)
    df = read_root(fname, key = "rechitntupler/hits", columns = branches, flatten = rh_branches )

    # set event column as index
    df.set_index('event', inplace = True)

    # #### Selection for TOA calibration
    sel = df.rechit_toaFall > 4
    #sel &= df.rechit_toaRise > 4 # just cross-check that the TOA is fine
    #sel &= df.rechit_energy > 1

    df['rechit_chip_id'] = df.rechit_chip + 4 * df.rechit_layer


    df_toa = df.loc[sel, ['rechit_chip_id', 'rechit_channel',
                          'rechit_energy','rechit_Tot','rechit_amplitudeLow',
                          'rechit_timeMaxLG', 'rechit_toaRise', 'rechit_toaFall'
                          ]
                    ]

    #hdfname = fname.replace(".root","") + '_LGTOT.h5'
    #outdir = '/eos/user/a/alobanov/HGCAL/testbeam/CERN2018/june/data/ntuples/ele100GeV/'
    outdir = '/eos/user/a/alobanov/HGCAL/testbeam/CERN2018/october/data/ntuples/v9/TOA/hdf_files/'
    #outdir = './'

    if not os.path.exists(outdir): os.makedirs(outdir)

    hdfname = outdir + os.path.basename(fname).replace(".root","") + '_TOA.h5'

    print('Writing %i rows to file %s' %(len(df_toa), hdfname))
    df_toa.to_hdf(hdfname,'table', complib='blosc:lz4', complevel=9, append = False)


if __name__ == "__main__":

    if len(sys.argv) > 1:
        fnames_ = sys.argv[1:]
    else:
        fnames_ = ["~/cernbox/HGCAL/testbeam/CERN2018/june/data/ntuples/ele100GeV/ntuple_381.root"]

    print('Found %i files' % len(fnames_))

    for fname in fnames_:
        process(fname)
